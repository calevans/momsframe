#!/bin/bash
#
# startSlide.sh
# Cal Evans <cal@calevans.com>
#
# startSlide.sh is called when the frame reboots. It does 2 things:
#   1. Pull new pix
#   2. Start the slide rotator
#
# On reboot, there are times when the network is not fully operational
# yet. Therefore we test for it. If we can use curl to access the site,
# the we know we have  a netowrk. Otherwise, we sleep for X seconds and
# then loop. We  By default, we loop 10 times before giving up. If the
# frame screen remains blank for more than 2 minutes, there is
# probably a newwork issue.
#
# On reboot, this is called from /etc/xdg/lxsession/LXDE-pi
#

#
# Initalize needed variables
#
DOMAIN=momspix.com
LOOP_COUNTER=1
MAX_LOOPS=10
SLEEP_TIME=12

#
# Make sure we have network or loop till we do
#
while [ "$LOOP_COUNTER" -lt "$MAX_LOOPS" ]
do
  curl -s https://$DOMAIN > /dev/null
  CURL_EXIT_CODE=$?
  if [ "$CURL_EXIT_CODE" -eq 0 ]
  then
    logger "startSlide.sh : Network is operational"
    break
  fi
  logger "startSlide.sh : Cannot reach $DOMAIN. Loop Counter = $LOOP_COUNTER"

  sleep $SLEEP_TIME

  LOOP_COUNTER=$((LOOP_COUNTER+1))
done

#
# If we counted more than the loop counter, bail. No net
#
if [ "$LOOP_COUNTER" -ge "$MAX_LOOPS" ]
then
  logger "startSlide.sh : Cannot find $DOMAIN"
  exit 1
fi

#
# Now that we know we have network, go ahead and bounce autossh.
#
sudo systemctl restart supervisor

#
# Now pull the pcitures
#
/home/calevans/momsframe/app/console.sh pull -q -c 40 --device jackie@momspix.com

logger "startSlide.sh : pull exited with code $?"

#
# Now restart the slideshow
#
killall slide
DISPLAY=:0.0 slide -r -t 300 -o 100 -p /opt/momspix > /dev/null &
exit 0