#! /bin/bash
phpcs --config-set default_standard PSR12 \
phpcs --config-set tab_width 4 \
phpcs --config-set colors 1 \
phpcs --config-set show_progress 1
