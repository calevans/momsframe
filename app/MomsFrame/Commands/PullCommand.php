<?php
declare(strict_types=1);
/**
 * @todo implement a real logger instead of passing around the output.
 */
namespace MomsFrame\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class PullCommand extends Command
{
    const CURRENT_IMAGE_PERCENTAGE = .6;

    protected $debug = false;
    protected $maxFiles = null;
    protected $output = null;
  /**
   * Called by the application, this method sets up the command.
   */
    protected function configure()
    {
        $definition = [
        new InputOption('count', 'c', InputOption::VALUE_REQUIRED, 'The number of pictures to refresh.'),
        new InputOption('device', '', InputOption::VALUE_REQUIRED, 'Email address that represents the device to pull for.'),
        ];

        $this->setName('pull')
           ->setDescription('Pull pictures to the Picture Frame')
           ->setDefinition($definition)
           ->setHelp(
               'This command is run on the picture frame and it pulls the ' .
               'appropriate number of pictures and places them in the local ' .
               'picture directory.'
           );
        return;
    }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        //$this->debug = $output->isDebug();
        $this->count = (int) $input->getOption('count');
        $this->device = (string) $input->getOption('device');

        $this->removeExistingImages();
        $imageCounter = 1;

        foreach($this->fetchImageList() as $image) {
            $this->output->writeln('Downloading image # ' . $imageCounter++, OutputInterface::VERBOSITY_DEBUG) ;
            $this->downloadImage($image);
        }

        $this->output->writeln('Done', OutputInterface::VERBOSITY_NORMAL) ;
        return 0;
    }

    private function removeExistingImages() : void
    {
        foreach (glob($this->getApplication()->config['pictureFrame']['pixDir'] . '*') as $key => $value) {
            unlink($value);
        }
    }

    private function fetchImageList() : array
    {
        $response = $this->getApplication()->config['guzzle']->request(
            'GET',
            $this->getApplication()->config['api']['urlRoot'] . 'wp-json/eicc/momspix/v1/pictures?deviceName='.$this->device."&count=".$this->count,
            [
                'auth' => [
                    $this->getApplication()->config['api']['user'],
                    $this->getApplication()->config['api']['password']
                ],
                'debug' => $this->debug,
                'headers' => [
                'Accept'     => 'application/json',
                ]
            ]
        );

        return json_decode($response->getBody()->getContents(), true);
    }

    private function downloadImage($image) : void
    {
            $fileName = $this->getApplication()->config['pictureFrame']['pixDir'] . basename($image);
            $response = $this->getApplication()->config['guzzle']->request(
                'GET',
                $image,
                [
                    'headers' => [
                    'Accept'     => 'application/json',
                ],
                'debug' => $this->debug,
                'http_errors' => false,
                'save_to' => $fileName
                ]
            );
    }
}
