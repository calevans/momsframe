#!/usr/bin/env php
<?php

declare(strict_types=1);
$appPath = realpath(dirname(__FILE__) . '/../') . '/';

require_once $appPath . 'vendor/autoload.php';

use MomsFrame\Commands\PullCommand;
use MomsFrame\Commands\WorkCommand;
use GuzzleHttp\Client;

use Symfony\Component\Console\Application;

$configFunc = require_once $appPath . "config/config.php";
$config = $configFunc($appPath);
$config['guzzle'] = new Client();
$app = new Application('MomsFrame', '1.0.0');

$app->config = $config;

$app->addCommands(
  [
    new PullCommand()
  ]
);

$app->run();
