# Operation Mom's Frame

(c) E.I.C.C., Inc.

Released under MIT license

All the code necessary to run ON A "Mom's Picture Frame".

All the actual processing now happens in the REST endpoint. This script just hits the endpoint and downloads the images listed.
